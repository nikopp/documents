\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{dfvlong}

% Example for passing text options to docmuent class
%\RequirePackage{kvoptions}
%\DeclareStringOption[Use DocTitle=title to set a title]{DocTitle}
%\title{\dfvlong@DocTitle}
%\ProcessKeyvalOptions*

% Option parsing

% Send all other options to scrartcl
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}
\ProcessOptions\relax

% Load default document class
\LoadClass[paper=a4,headinclude=true, headheight=78pt, footheight=21pt]{scrartcl}

% Set geometry of document
\usepackage[top=3cm, bottom=3cm, left=2.5cm, right=2.5cm, head=26pt]{geometry}
\usepackage{xstring}  % StrReplace

% CC Logos
\usepackage{ccicons}

% Use polyglossia with new german spelling py default
\RequirePackage{polyglossia} 
\setdefaultlanguage[spelling=new, babelshorthands=true]{german}

% Font set up
\RequirePackage{fontspec} 
\RequirePackage[sfdefault]{roboto}
\renewcommand{\familydefault}{\sfdefault}

\RequirePackage[bookmarks,pdfencoding=auto]{hyperref}
\RequirePackage{scrdate}

% Header and Footer
\RequirePackage[headsepline]{scrlayer-scrpage}
\pagestyle{scrheadings}
\automark{section}
\clearpairofpagestyles
\lohead{\includegraphics[scale=0.3]{dfv-logo.pdf}}
\chead{\headmark}
\rohead{\textbf{\normalfont\textsf{\@title}}}

\lofoot{DFV-\StrSubstitute{\@title}{ }{\_}-\@date}
\rofoot{\pagemark}

% Title Page
\renewcommand{\maketitle}{%
  \raggedright
  \begin{center}
  \includegraphics[width=.6\linewidth]{dfv-logo.pdf}\\[8ex]
    {\Huge \bfseries \sffamily \@title }\\[4ex]
%    {\Large \@author}\\[4ex]
    {\huge v. \@date}\\[8ex]
  \end{center}
}



