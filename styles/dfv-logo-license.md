# License und usage of DFV-Logo

From [here](http://www.frisbeesportverband.de/wp-content/uploads/2014/09/DFV-Logo_CI_Januar2018.pdf), visited on 2019-01-04:

## Beschreibung:
Das neue Logo des Deutschen Frisbeesport-Verbandes (DFV e.V.) stellt eine leicht geneigte Frisbeescheibe in einem Blauton dar. Die Frisbeescheibe ist durch zwei Kreisformen dargestellt, wobei der äußere Kreis geschlossen und der innere Kreis oben und unten jeweils etwas geöffnet ist.

Die Schriftart ist „Aller“ regular (Lizenz: https://www.fontsquirrel.com/license/Aller, 100% free for commercial use).

Der Farbwert des Blautons lautet: CMYK (100, 81, 0, 0) bzw. RGB (9, 76, 160).

## Interpretation:
Das neue DFV-Logo verdeutlicht auf einen Blick den Namen und den Zuständigkeitsbereich des Deutschen Frisbeesport-Verbands als nationaler Spitzensportverband für alle mit Frisbeescheiben ausgeübten Sportarten.

In der Darstellung der Frisbeescheibe erzeugt die Schrägstellung eine Dynamik, die der vielfältigen Bewegung einer Frisbeescheibe durch geschicktes Werfen im Flug entspricht; der innere, zweimal durchbrochene Kreis erzeugt eine weitere Dynamik, die an die Rotation einer Frisbeescheibe denken lässt.

## Verwendung:
Das Logo wird grundsätzlich immer als Wort-Bild-Marke verwendet. Eine Verwendung des Logos in Kombination mit einem anderen oder erweiterten Schriftzug ist nicht vorgesehen.

Eine schwarz-weiße Version des Logos ist ebenfalls möglich. Sie ist nur in Sonderfällen vorgesehen, bzw. ergibt sich automatisch beim schwarz-weißen Ausdruck von Briefen, Mails oder Dokumenten des DFV.

Es ist nicht erlaubt, auf das Logo andere Schriften oder Grafiken zu applizieren. Es ist stets allein stehend zu platzieren und darf keine anderen Veränderungen enthalten.

Die einzige Ausnahme in der sonstigen Verwendung des Logos als Wort-Bild-Marke ist das alleingestellte Scheibenlogo als quadratisches Icon für die Darstellung des DFV in Sozialen Medien. Das Logo ist als jpg- und als pdf-Datei hinterlegt auf der Seite www.frisbeesportverband.de/index.php/verband/organisationsstuktur/orga-und-docs-01/.

## Varianten
Aktuell sind keine Varianten des DFV-Logos vorhanden.

*Jörg Benner, Geschäftsführer DFV, Januar 2018*